<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInit929ab1b95392d49bd0f9bc09093146a5
{
    public static $prefixLengthsPsr4 = array (
        'P' => 
        array (
            'PHPMailer\\PHPMailer\\' => 20,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'PHPMailer\\PHPMailer\\' => 
        array (
            0 => __DIR__ . '/..' . '/phpmailer/phpmailer/src',
        ),
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInit929ab1b95392d49bd0f9bc09093146a5::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInit929ab1b95392d49bd0f9bc09093146a5::$prefixDirsPsr4;

        }, null, ClassLoader::class);
    }
}
