$( document ).ready(function() {
    var $aboutTitle = $('.about-myself .content h2');
    var $developmentWrapper = $('.development-wrapper');
    var developmentIsVisible = false;


    /* ####### HERO SECTION ####### */
    $('.hero .content .header').delay(500).animate({
        'opacity':'1',
        'top': '50%'
    },1000);


    $(window).scroll( function(){

        var bottom_of_window = $(window).scrollTop() + $(window).height();

        /* ##### ABOUT MYSELF SECTION #### */
        if( bottom_of_window > ($aboutTitle.offset().top + $aboutTitle.outerHeight())){
            $('.about-myself .content h2').addClass('aboutTitleVisible');
        }

        /* ##### EXPERIENCE SECTION #### */
        $('.experience .content .hidden').each( function(i){

            var bottom_of_object = $(this).offset().top + $(this).outerHeight();

            if( bottom_of_window > bottom_of_object ){

                $(this).animate({
                    'opacity':'1',
                    'margin-left': '0'
                },600);
            }
        });

        /*###### SKILLS SECTION ######*/
        var middle_of_developmentWrapper = $developmentWrapper.offset().top + $developmentWrapper.outerHeight()/2;

        if((bottom_of_window > middle_of_developmentWrapper)&& (developmentIsVisible == false)){

            $('.skills-bar-container li').each( function(){

                var $barContainer = $(this).find('.bar-container');
                var dataPercent = parseInt($barContainer.data('percent'));
                var elem = $(this).find('.progressbar');
                var percent = $(this).find('.percent');
                var width = 0;
                var id = setInterval(frame, 15);

                function frame() {
                    if (width >= dataPercent) {
                        clearInterval(id);
                    } else {
                        width++;
                        elem.css("width", width+"%");
                        percent.html(width+" %");
                    }
                }
            });
            developmentIsVisible = true;
        }
    });
});

/*#### MOBILE NAV ####*/
$('.mobile-toggle').click(function(){
    if($('#main-header').hasClass('open-nav')){
        $('#main-header').removeClass('open-nav');
    }else{
        $('#main-header').addClass('open-nav');
    }
});

/*#### NAV SCROLL ####*/
$('nav a').click(function(event) {
    var id = $(this).attr("href");
    var offset = 70;
    var target = $(id).offset().top - offset;
    $('html, body').animate({
        scrollTop: target
    }, 500);
    event.preventDefault();
});


/*#### SCROLLMAGIC SECTION NAV ####*/
var controller = new ScrollMagic.Controller();

new ScrollMagic.Scene({triggerElement: "#hero2", duration: $("#hero").height()}).setClassToggle("#hero2-link", "active").addTo(controller);
new ScrollMagic.Scene({triggerElement: "#about", duration: $("#about").height() + 100}).setClassToggle("#about-link", "active").addTo(controller);
new ScrollMagic.Scene({triggerElement: "#experience", duration: $("#experience").height()}).setClassToggle("#experience-link", "active").addTo(controller);
new ScrollMagic.Scene({triggerElement: "#competence", duration: $("#competence").height()}).setClassToggle("#competence-link", "active").addTo(controller);
new ScrollMagic.Scene({triggerElement: "#projet", duration: $("#projet").height() + 250}).setClassToggle("#projet-link", "active").addTo(controller);
new ScrollMagic.Scene({triggerElement: "#contact", duration: $("#contact").height()}).setClassToggle("#contact-link", "active").addTo(controller);


/*#### IMAGE PROJET ####*/
var $grid = $('.img-grid').isotope({
    itemSelector: '.img-container',
});


// FILTRE FUNCTIONS
var filterFns = {
    numberGreaterThan50: function() {

    },
};

// FILTER BUTTON CLICK
$('#filter-btn').on( 'click', 'button', function() {
    var filterValue = $( this ).attr('data-filter');
    filterValue = filterFns[ filterValue ] || filterValue;
    $grid.isotope({ filter: filterValue });
});

// CHANGE CLASS ON CLICK
$('.button-group').each( function( i, buttonGroup ) {
    var $buttonGroup = $( buttonGroup );
    $buttonGroup.on( 'click', 'button', function() {
        $buttonGroup.find('.is-checked').removeClass('is-checked');
        $( this ).addClass('is-checked');
    });
});

/*#### CONTACT ####*/
var _gaq = _gaq || [];
_gaq.push(['_setAccount', 'UA-43981329-1']);
_gaq.push(['_trackPageview']);
(function() {
    var ga = document.createElement('script');
    ga.type = 'text/javascript';
    ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(ga, s);
})();
